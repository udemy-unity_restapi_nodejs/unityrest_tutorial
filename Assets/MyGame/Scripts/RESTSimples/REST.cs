using System;
using System.Collections;
using System.Collections.Generic;
using MyGame.Scripts.Utilities;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;
using Object = UnityEngine.Object;

public class REST : MonoBehaviour
{
    [SerializeField] private string webURL;
    [SerializeField] private string paramsName = "";
    [SerializeField] private RESTOps operation;
    [Header("Form Data")]
    [SerializeField] private Object playerData;
    
    private string playerRoute = "player";

    public enum RESTOps
    {
        Get, Post, Put, Delete
    }


    private void Start()
    {
        switch (operation)
        {
            case RESTOps.Get:
                Get<PlayerList>(webURL, playerRoute, paramsName, GetPlayers);
                break;
            case RESTOps.Post:
                Post(playerData, webURL, playerRoute);
                break;
            case  RESTOps.Put:
                PutCoroutine(playerData, webURL, paramsName, playerRoute);
                break;
            case RESTOps.Delete:
                DeleteCoroutine(webURL, playerRoute, paramsName);
                break;
        }
    }

    private static IEnumerator GetCoroutine<T>(string baseURL, string route, string identifier, Action<T> callback)
    {
        string fullURL = baseURL.BuildUrl(route, identifier);
        using UnityWebRequest webRequest = UnityWebRequest.Get(fullURL);
        yield return webRequest.SendWebRequest();

        if (webRequest.result == UnityWebRequest.Result.ConnectionError)
        {
            Debug.Log(webRequest.error);
        }
        else
        {
            if (webRequest.isDone)
            {
                string jsonResult = System.Text.Encoding.UTF8.GetString(webRequest.downloadHandler.data);
                if (callback != null)
                {
                    // PlayerList playerList = JsonUtility.FromJson<PlayerList>("{\"players\":" + jsonResult + "}");
                    var receivedObj = JsonUtility.FromJson<T>(jsonResult);
                    callback(receivedObj);
                }
                else
                {
                    Debug.Log(jsonResult);
                }
            }
        }
    }//GET
    public void Get<T>(string baseURL, string route = "", string parameters = "", Action<T> callback = null)
        => StartCoroutine(GetCoroutine<T>(baseURL, route, parameters, callback));

    private IEnumerator GetAudio(string url)
    {
        UnityWebRequest webRequest = UnityWebRequestMultimedia.GetAudioClip("e", AudioType.WAV);
        yield return webRequest.SendWebRequest();

        if (webRequest.result == UnityWebRequest.Result.ConnectionError)
        {
            Debug.Log(webRequest.error);
        }
        else
        {
            if (webRequest.isDone)
            {
                AudioClip audioResult = DownloadHandlerAudioClip.GetContent(webRequest);
                // Debug.Log(jsonResult);
            }

        }
    }
    private void AudioGetCoroutine(string url) => StartCoroutine(GetAudio(url));

    private IEnumerator PostCoroutine(object obj, string baseURL,string route)
    {
        string fullURL = baseURL.BuildUrl(route);
        string jsonData = JsonConvert.SerializeObject(obj);
        Debug.Log(jsonData);

        using (UnityWebRequest webRequest = UnityWebRequest.Post(fullURL, jsonData))
        {
            webRequest.SetRequestHeader("content-type", "application/json");
            webRequest.uploadHandler.contentType = "application/json";
            webRequest.uploadHandler = new UploadHandlerRaw(System.Text.Encoding.UTF8.GetBytes(jsonData));

            yield return webRequest.SendWebRequest();

            if (webRequest.result == UnityWebRequest.Result.ConnectionError)
            {
                Debug.Log(webRequest.error);
            }
            else
            {
                if (webRequest.isDone)
                {
                    string jsonResult = System.Text.Encoding.UTF8.GetString(webRequest.downloadHandler.data);
                    Debug.Log(jsonResult);
                }
            }
        }
    }//POST
    private void Post(object newPlayer, string baseUrl, string route = "")
        => StartCoroutine(PostCoroutine(newPlayer, baseUrl, route));
    
    private IEnumerator Put(object obj, string baseURL,string route, string identifier)
    {
        playerData.name = identifier;
        string fullURL = baseURL.BuildUrl(route, identifier);
        string jsonData = JsonConvert.SerializeObject(obj);

        using (UnityWebRequest webRequest = UnityWebRequest.Put(fullURL, jsonData))
        {
            webRequest.SetRequestHeader("content-type", "application/json");
            webRequest.uploadHandler.contentType = "application/json";
            webRequest.uploadHandler = new UploadHandlerRaw(System.Text.Encoding.UTF8.GetBytes(jsonData));

            yield return webRequest.SendWebRequest();

            if (webRequest.result == UnityWebRequest.Result.ConnectionError)
            {
                Debug.Log(webRequest.error);
            }
            else
            {
                if (webRequest.isDone)
                {
                    string jsonResult = System.Text.Encoding.UTF8.GetString(webRequest.downloadHandler.data);
                    Debug.Log(jsonResult);
                }
            }
        }
    }//PUT
    private void PutCoroutine(object obj, string baseUrl, string identifier, string route = "")
        => StartCoroutine(Put(obj, baseUrl, route, identifier));
    
    private IEnumerator Delete(string baseURL, string route, string identifier)
    {
        string fullURL = baseURL.BuildUrl(route, identifier);
        using UnityWebRequest webRequest = UnityWebRequest.Delete(fullURL);
        yield return webRequest.SendWebRequest();

        if (webRequest.result == UnityWebRequest.Result.ConnectionError)
        {
            Debug.Log(webRequest.error);
        }
        else
        {
            if (webRequest.isDone)
            {
                Debug.Log("WebRequest ResponseCode: " + webRequest.responseCode);
                string jsonResult = System.Text.Encoding.UTF8.GetString(webRequest.downloadHandler.data);
                Debug.Log(jsonResult);
            }
        }
    }//DELETE
    private void DeleteCoroutine(string baseURL, string route = "", string parameters = "")
        => StartCoroutine(Delete(baseURL, route, parameters));

    private void GetPlayers(PlayerList playerList)
    {
        foreach (var player in playerList.players)
        {
            Debug.Log(player.ToString());
        }
    }
    
    
}
