using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Player
{
    public string name;
    public string level;
    public int score;

    public override string ToString()
    {
        return
            " Player Name: " + name +
            "\r\n Player Level: " + level +
            "\r\n Player Score: " + score;
    }
}
