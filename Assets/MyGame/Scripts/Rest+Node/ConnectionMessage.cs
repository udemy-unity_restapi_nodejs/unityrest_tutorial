[System.Serializable]
public class ConnectionMessage
{
    public int status;
    public string message;

    private string messageDefault = "There's been a problem. Try again in some minutes";

    public ConnectionMessage(int status, string message = null)
    {
        this.status = status;
        this.message = message ?? messageDefault;
    }

    public int Status
    {
        get => status;
        set => status = value;
    }

    public string Message
    {
        get => string.IsNullOrEmpty(message) && status != 200 ? messageDefault : message;
        set => message = value;
    }

    public override string ToString()
    {
        return
            $"Status: {Status}\n" +
            $"Message: {Message}";
    }
}
