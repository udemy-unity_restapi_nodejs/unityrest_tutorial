[System.Serializable]
public class Login
{
    private string email;
    private string password;

    public Login(string p_email, string p_password)
    {
        this.email = p_email;
        this.password = p_password;
    }

    public string Email
    {
        get => email;
        set => email = value;
    }

    public string Password
    {
        get => password;
        set => password = value;
    }
}
