using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEditor.VersionControl;
using UnityEngine;
using UnityEngine.Networking;

public class RestController : MonoBehaviour
{
    [SerializeField] private string webURL = "localhost:3000";
    [SerializeField] private string routeLogin = "/login";
    [SerializeField] private string routeRegister = "/register";

    private void Start()
    {
        // SendRESTGetLogin("lol", "teste");
        SendRESTPostRegister("EMAIL@email", "qwert");
        
    }

    #region  ################## PUBLIC METHODS #############
    public void SendRESTGetLogin(Login login, Action<ConnectionMessage> callback = null)
    {
        StartCoroutine(LoginGet(webURL, routeLogin, login, callback));
    }
    public void SendRESTGetLogin(string email, string password, Action<ConnectionMessage> callback = null)
    {
        SendRESTGetLogin(new Login(email, password), callback);
    }
    
    public void SendRESTPostRegister(Login login, Action<ConnectionMessage> callback = null)
    {
        StartCoroutine(RegisterPost(webURL, routeRegister, login, callback));
    }
    public void SendRESTPostRegister(string email, string password, Action<ConnectionMessage> callback = null)
    {
        SendRESTPostRegister(new Login(email, password), callback);
    }
    
    #endregion

    #region ########### LOGIN GET ############
    public IEnumerator LoginGet(string url, string route, Login loginPlayer, Action <ConnectionMessage> callback)
    {
        // string newUrl = "localhost:3000/login/teste@teste.com/12345";
        string newUrl = $"{url}{route}/{loginPlayer.Email}/{loginPlayer.Password}";
        
        Debug.Log("NewURL: " + newUrl);

        using (UnityWebRequest www = UnityWebRequest.Get(newUrl))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                var msg_err = new ConnectionMessage((int) www.responseCode, www.error);
                Debug.Log(msg_err);
                callback?.Invoke(msg_err);
            }
            else
            {
                if (www.isDone)
                {
                    string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    Debug.Log("JsonResult: " + jsonResult);
                    var msg_res = JsonUtility.FromJson<ConnectionMessage>(jsonResult);
                    callback?.Invoke(msg_res);

                }
            }
        }
    }
    #endregion

    #region ########### REGISTER POST ##########
    public IEnumerator RegisterPost(string url, string route, Login loginPlayer, Action <ConnectionMessage> callback)
    {
        // string newUrl = "localhost:3000/register";
        string newUrl = $"{url}{route}";
        
        Debug.Log("NewURL: " + newUrl);
        
        string jsonData = JsonConvert.SerializeObject(loginPlayer).ToLower();
        Debug.Log("jsonData: " + jsonData);

        using (UnityWebRequest www = UnityWebRequest.Post(newUrl, jsonData))
        {
            www.SetRequestHeader("content-type", "application/json");
            www.uploadHandler.contentType = "application/json";
            www.uploadHandler = new UploadHandlerRaw(System.Text.Encoding.UTF8.GetBytes(jsonData));
            
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                var msg_err = new ConnectionMessage((int) www.responseCode, www.error);
                Debug.Log(msg_err);
                callback?.Invoke(msg_err);
            }
            else
            {
                if (www.isDone)
                {
                    string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    Debug.Log("JsonResult: " + jsonResult);
                    var msg_res = JsonUtility.FromJson<ConnectionMessage>(jsonResult);
                    callback?.Invoke(msg_res);

                }
            }
        }
    }
    

    #endregion
}
