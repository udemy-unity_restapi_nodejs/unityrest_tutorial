using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    [Header("Login")] 
    [SerializeField] private GameObject login_canvas;
    [SerializeField] private TMP_InputField login_email;
    [SerializeField] private TMP_InputField login_password;
    [SerializeField] private Button login_button;
    [SerializeField] private Button login_newreg_button;
    
    [Header("Register")] 
    [SerializeField] private GameObject register_canvas;
    [SerializeField] private TMP_InputField register_email;
    [SerializeField] private TMP_InputField register_password;
    [SerializeField] private TMP_InputField register_rePassword;
    [SerializeField] private Button register_button;
    [SerializeField] private Button register_back_button;
    
    [Header("Message")] 
    [SerializeField] private GameObject message_canvas;
    [SerializeField] private TextMeshProUGUI message_text;
    [SerializeField] private Button message_button;

    [Header("Message")] 
    [SerializeField] private GameObject menuGame_canvas;

    [Space] [Header("REST")] 
    [SerializeField] private RestController restController;

    private GameObject[] canvases;
    private string menu_email;
    private string menu_password;
    
    private void Awake()
    {
        canvases = new[] {login_canvas, register_canvas, message_canvas, menuGame_canvas};

        menu_email = "";
        menu_password = "";
        
        //Login
        login_button.onClick.AddListener(Button_Login);
        login_newreg_button.onClick.AddListener(Button_Login_NewRegister);

        login_email.text = PlayerPrefs.GetString("PLAYER_EMAIL", "");
        login_password.text = PlayerPrefs.GetString("PLAYER_PASSWORD", "");

        //Message 
        message_button.onClick.AddListener(Button_MessageClose);
        
        //Register
        register_button.onClick.AddListener(Button_Register);
        register_back_button.onClick.AddListener(Button_RegisterBack);
        
        MenuActive(login_canvas);
    }
    
    #region ############### FUNCTIONS ############
    
    //Função que gerencia o canvas que será ativado
    void MenuActive(GameObject selectedCanvas)
    {
        Array.ForEach(canvases, c => c.SetActive(c.name.Equals(selectedCanvas.name)));    
    }

    //Função para receber os dados no formato ConnectionMessage;
    void GetMessage(ConnectionMessage msg)
    {
        if (!string.IsNullOrEmpty(msg.Message))
        {
            message_text.text = msg.Message;
            message_canvas.gameObject.SetActive(true);
            return;
        }

        StartGame();
    }

    void StartGame()
    {
        if (menu_email != "" && menu_password != "")
        {
            //Gravar os dados no PlayerPrefs para preenchimento automático
            PlayerPrefs.SetString("PLAYER_EMAIL", menu_email);
            PlayerPrefs.SetString("PLAYER_PASSWORD", menu_password);
        }
        else
        {
            Debug.LogWarning("Erro ao tentar salvar E-mail e Senha no PlayerPrefs");
        }
        
        //Se estiver tudo certo, inciar jogo
        MenuActive(menuGame_canvas);
    }

    private string Encrypt(string password)
    {
        MD5 md5 = MD5.Create();
        string result = "";
        byte[] data = md5.ComputeHash(Encoding.UTF8.GetBytes(password));

        for (int i = 0; i < data.Length; i++)
        {
            result += data[i].ToString("x2");
        }
        
        Debug.Log("Encrypted password: " + result);

        return result;
    }
    
    #endregion
    
    #region ############## LOGIN ############

    private void Button_Login()
    {
        bool err = false;

        string email_temp = login_email.text;
        string password_temp = login_password.text;

        if (email_temp == "")
        {
            message_text.text = "Digite um e-mail!";
            err = true;
        }
        else if (password_temp == "")
        {
            message_text.text = "Digite uma senha!";
            err = true;
        }

        if (err)
        {
            message_canvas.SetActive(true);
            return;
        }
        
        menu_email = email_temp;
        menu_password = password_temp;

        password_temp = Encrypt(password_temp);
        
        //Envio REST
        Login_Send(email_temp, password_temp);
    }

    //Ativa a janela de cadastro
    private void Button_Login_NewRegister()
    {
        MenuActive(register_canvas);
    }
    
    #endregion

    #region ############## REGISTER ############

    private void Button_Register()
    {
        bool err = false;

        string email_temp = register_email.text;
        string password_temp = register_password.text;
        string rePassword_temp = register_rePassword.text;

        if (email_temp == "")
        {
            message_text.text = "Digite um e-mail!";
            err = true;
        }

        else if (password_temp == "" && !err)
        {
            message_text.text = "Digite uma senha!";
            err = true;
        }
        
        else if (rePassword_temp == "" && !err)
        {
            message_text.text = "Digite a senha novamente!";
            err = true;
        }

        else if (!password_temp.Equals(rePassword_temp) && !err)
        {
            message_text.text = "A senha não confere!";
            err = true;
        }

        if (err)
        {
            message_canvas.SetActive(true);
            return;
        }
        
        menu_email = email_temp;
        menu_password = password_temp;

        password_temp = Encrypt(password_temp);

        //Aqui chamaremos a função de envio dos dados para o servidor que vai ativar ou não a próxima janela
        Register_Send(email_temp, password_temp);
    }

    void Button_RegisterBack()
    {
        MenuActive(login_canvas);
    }
    
    #endregion
    
    #region ############# MENSAGEM ##########
    //Fecha nossa janela de mensagem
    void Button_MessageClose()
    {
        message_canvas.SetActive(false);
    }
    #endregion

    #region ########## REST_FUNCTIONS ##########

    void Login_Send(string email, string password)
    {
        restController.SendRESTGetLogin(email, password, GetMessage);
    }

    void Register_Send(string email, string password)
    {
        restController.SendRESTPostRegister(email, password, GetMessage);
    }


    #endregion

}
