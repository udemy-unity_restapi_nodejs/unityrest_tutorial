﻿using UnityEngine;

namespace MyGame.Scripts.Utilities
{
    public static class StringExtensions
    {
        public static string BuildUrl(this string baseUrl, params string[] routeComplements)
        {
            foreach (var route in routeComplements)
            {
                if (route.Equals(""))
                {
                    break;
                }

                if (route.StartsWith("/"))
                {
                    baseUrl += route;
                }
                else
                {
                    baseUrl += $"/{route}";
                }
            }
            return baseUrl;
        }
    }
}